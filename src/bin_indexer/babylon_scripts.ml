let get_script cctxt k =
  Tezos_protocol_005_PsBabyM1.Protocol.Alpha_services.Contract.info
    cctxt ((`Main, `Level 655360l):Block_services.chain * Block_services.block) k >>=? fun
    {
      balance = _ ; delegate = _ ; counter = _ ; script ;
    } ->
  return script

let update_scripts cctxt conn =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.collect_list Tezos_indexer_005_PsBabyM1.Db_alpha.Contract_table.get_scriptless_contracts ()
  >>= Db_shell.caqti_or_fail >>= fun scriptless_contracts ->
  Lwt_list.iter_s
    (fun k ->
       get_script cctxt k >>= function
       | Ok (Some s) ->
         Conn.exec Tezos_indexer_005_PsBabyM1.Db_alpha.Contract_table.update_script (k, s)
         >>= Db_shell.caqti_or_fail
       | _ -> Lwt.return_unit
    )
    scriptless_contracts
