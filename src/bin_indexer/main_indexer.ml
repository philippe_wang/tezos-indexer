(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module RPC_client = Tezos_rpc_http_client_unix.RPC_client_unix

open Cmdliner

open Tezos_protocol_007_PsDELPH1.Protocol
open Tezos_baking_007_PsDELPH1

let default_db_url =
  Uri.make ~scheme:"postgresql" ~host:"localhost" ~path:"chain" ()

exception Exit_watch of error list

let first_block_level =
  let doc = "Specify level of first alpha block" in
  Arg.(value & opt int32 2l & info ~doc ["first-block"])

open Protocol_heights

let mainnet_protocol_heights = {
  proto_001 =  28082l ;
  proto_002 = 204761l;
  proto_003 = 458752l;
  proto_004 = 655360l;
  proto_005 = 851968l;
  proto_006 = 1212416l;
  proto_007 = Int32.max_int;
  proto_008 = Int32.max_int;
}

let carthagenet_protocol_heights = {
  proto_001 = 0l ;
  proto_002 = 0l;
  proto_003 = 0l;
  proto_004 = 0l;
  proto_005 = 36864l;
  proto_006 = Int32.max_int;
  proto_007 = Int32.max_int;
  proto_008 = Int32.max_int;
}

let delphinet_protocol_heights = {
  proto_001 = 0l ;
  proto_002 = 0l;
  proto_003 = 0l;
  proto_004 = 0l;
  proto_005 = 0l;
  proto_006 = 0l;
  proto_007 = Int32.max_int;
  proto_008 = Int32.max_int;
}

let edonet_protocol_heights = {
  proto_001 = 0l ;
  proto_002 = 0l;
  proto_003 = 0l;
  proto_004 = 0l;
  proto_005 = 0l;
  proto_006 = 0l;
  proto_007 = 0l;
  proto_008 = Int32.max_int;
}


let hash_1 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
let hash_2 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
let hash_3 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
let hash_4 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
let hash_5 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
let hash_6 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
let hash_7 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
let hash_8 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"


let bootstrap ?use_disk_cache ~conn ~cctxt ~first_alpha_level ?up_to ~heights ?push_start_snapshots ?push_start_watch () =
  Verbose.printf "# Starting bootstrap";
  begin
    match up_to with
    | Some up_to -> Lwt.return up_to
    | None ->
      Client_baking_blocks.info cctxt (`Head 0) >>= function
      | Error err ->
        Verbose.error "# Error when trying to bootstrap: %a" pp_print_error err;
        exit Verbose.ExitCodes.failure_bootstrap
      | (Ok { Client_baking_blocks.level ; _ }) ->
        Lwt.return (Alpha_context.Raw_level.to_int32 level)
  end >>= fun up_to ->
  Verbose.printf "# Target_block=%ld" up_to;
  let rec fold f (accu:int32) = function
    | [] ->
      return accu
    | e::tl ->
      f accu e >>=? fun accu ->
      fold f accu tl
  in
  let proto_list =
    [
      Tezos_indexer_001_PtCJ7pwo.Chain_db.bootstrap_chain, min up_to heights.proto_001
    ; Chain_db_transition_001_002.bootstrap_chain, min up_to @@ Int32.succ heights.proto_001
    ; Tezos_indexer_002_PsYLVpVv.Chain_db.bootstrap_chain, min up_to heights.proto_002
    ; Chain_db_transition_002_003.bootstrap_chain, min up_to @@ Int32.succ heights.proto_002
    ; Tezos_indexer_003_PsddFKi3.Chain_db.bootstrap_chain, min up_to heights.proto_003
    ; Chain_db_transition_003_004.bootstrap_chain, min up_to @@ Int32.succ heights.proto_003
    ; Tezos_indexer_004_Pt24m4xi.Chain_db.bootstrap_chain, min up_to heights.proto_004
    ; Chain_db_transition_004_005.bootstrap_chain, min up_to @@ Int32.succ heights.proto_004
    ; (fun ?use_disk_cache:_ ?from:_ ?up_to:_ ~first_alpha_level:_ cctxt conn ->
        (* Once Babylon gets activated on mainnet, scriptless accounts are assigned a default
           script. Here we fill that void. *)
        Babylon_scripts.update_scripts cctxt conn >>= fun _ ->
        return 655360l
      ), min up_to @@ Int32.succ heights.proto_004
    ; Tezos_indexer_005_PsBabyM1.Chain_db.bootstrap_chain, min up_to heights.proto_005
    ; Chain_db_transition_005_006.bootstrap_chain, min up_to @@ Int32.succ heights.proto_005
    ; Tezos_indexer_006_PsCARTHA.Chain_db.bootstrap_chain, min up_to heights.proto_006
    ; Chain_db_transition_006_007.bootstrap_chain, min up_to @@ Int32.succ heights.proto_006
    ; Tezos_indexer_007_PsDELPH1.Chain_db.bootstrap_chain, min up_to heights.proto_007
    ; Tezos_indexer_008_PtEdoTez.Chain_db.bootstrap_chain, min up_to heights.proto_008
    ]
  in
  fold
    (fun last_treated_block
      ((f : (?use_disk_cache:string ->
             ?from:int32 ->
             ?up_to:int32 ->
             first_alpha_level:int32 ->
             Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full ->
             (module Caqti_lwt.CONNECTION) -> int32 tzresult Lwt.t)),
       up_to)
      ->
        Verbose.Debug.printf "# %s last_treated_block=%ld" __LOC__
          last_treated_block;
        f
          ?use_disk_cache
          ~up_to
          ~first_alpha_level
          cctxt
          conn
    )
    first_alpha_level
    proto_list
  >>=? fun last_treated_block ->
  Verbose.printf "# last_treated_block=%ld" last_treated_block;
  Option.iter (fun e -> Lwt.wakeup e last_treated_block) push_start_watch ;
  Option.iter (fun e -> Lwt.wakeup e ())  push_start_snapshots;
  return_unit

let record_action pool =
  let argv = Array.fold_left (Printf.sprintf "%s %S") "" Sys.argv in
  Caqti_lwt.Pool.use
    (fun dbh ->
       let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
       Conn.exec Db_shell.Indexer_log.record
         (Version.(Printf.sprintf "%s %s %s" t b c), argv)
    )
    pool

let snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt =
  if no_snapshot_blocks then
    return ()
  else
    let rec get_snapshots cycle =
      (
        Snapshot_utils_6.get_roll_snapshot_for_cycle cycle cctxt
        >>= function
        | Error _ ->
          Snapshot_utils_7.get_roll_snapshot_for_cycle cycle cctxt
        | Ok x -> return x
      )
      >>= function
      | Ok block ->
        let block = Int32.of_int block in
        Verbose.printf "# Snapshot: cycle=%d block=%ld" cycle block;
        Tezos_indexer_lib.Db_shell.Snapshot_table.store_snapshot_levels
          pool [cycle, block]
        >>= fun () ->
        Verbose.printf "# Getting next snapshot with cycle=%d" (succ cycle);
        get_snapshots (succ cycle)
      | Error _ ->
        Lwt_unix.sleep 60. >>= fun () ->
        get_snapshots cycle
    in
    (if snapshot_blocks_only then
       Lwt.return_unit
     else
       start_snapshots) >>= fun () ->
    get_snapshots 7

let watch cctxt conn rejected_blocks_depth keep_rejected use_disk_cache first_alpha_level heights start_watch =
  let fold_f block result =
    match block, result with
    | _, Error err
    | Error err, _ ->
      Verbose.error "%a" pp_print_error err ;
      Lwt.return (Error err)
    | Ok ({ Client_baking_blocks.hash ;
            chain_id = _; predecessor = _; fitness = _; timestamp = _;
            protocol ; next_protocol ; proto_level = _; level ;
            context = _;
          }), Ok () ->
      match
        List.assoc_opt (protocol, next_protocol)
          [
            (* most probable *)
            (hash_7, hash_7),
            (fun () -> Tezos_indexer_007_PsDELPH1.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn);
            (hash_8, hash_8),
            (fun () -> Tezos_indexer_008_PtEdoTez.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn);
            (hash_6, hash_6),
            (fun () -> Tezos_indexer_006_PsCARTHA.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_6, hash_7),
            (fun () -> Chain_db_transition_006_007.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn);
            (* least probable *)
            (hash_5, hash_5),
            (fun () -> Tezos_indexer_005_PsBabyM1.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_4, hash_4),
            (fun () -> Tezos_indexer_004_Pt24m4xi.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_3, hash_3),
            (fun () -> Tezos_indexer_003_PsddFKi3.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_2, hash_2),
            (fun () -> Tezos_indexer_002_PsYLVpVv.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_1, hash_1),
            (fun () -> Tezos_indexer_001_PtCJ7pwo.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (* even less probable, since these transitions happen at most once per chain *)
            (hash_5, hash_6),
            (fun () -> Chain_db_transition_005_006.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_4, hash_5),
            (fun () -> Chain_db_transition_004_005.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_3, hash_4),
            (fun () -> Chain_db_transition_003_004.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_2, hash_3),
            (fun () -> Chain_db_transition_002_003.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
            (hash_1, hash_2),
            (fun () -> Chain_db_transition_001_002.store_block_full
                ~block:(`Hash (hash, 0)) cctxt conn) ;
          ]
      with
      | Some f ->
        f () >>= fun _ ->
        let module Conn = (val conn : Caqti_lwt.CONNECTION) in
        if rejected_blocks_depth > 0l then
          begin
            Conn.collect_list Db_shell.Rejected_blocks.mark (rejected_blocks_depth)
            >>=
            Db_shell.caqti_or_fail >>= fun l ->
            List.iter (fun (bh, level) ->
                Verbose.printf "> event=update block_level=%d block=%a type=rejected_block"
                  level Block_hash.pp bh
              ) l;
            if l <> [] && not keep_rejected then
              begin
                Conn.find_opt Db_shell.Rejected_blocks.delete () >>=
                Caqti_lwt.or_fail >>= fun up_to ->
                let up_to = match up_to with
                  | Some up_to -> up_to
                  | None -> Alpha_context.Raw_level.to_int32 level
                in
                bootstrap
                  ?use_disk_cache
                  ~conn ~cctxt ~first_alpha_level
                  ~up_to ~heights ()
                >>= fun _ ->
                Lwt.return (Ok ())
              end
            else
              Lwt.return (Ok ())
          end
        else
          Lwt.return (Ok ())
      | None -> Lwt.fail (Failure "unsupported protocol")
  in
  Verbose.printf "# Start watching";
  begin
    Client_baking_blocks.monitor_valid_blocks
      cctxt ~next_protocols:None ()
    >>=? fun stream ->
    Verbose.printf "# Start monitoring blocks";
    Lwt.catch begin fun () ->
      Lwt_stream.peek stream >>= function
      | None ->
        Verbose.error "failed to peak stream";
        (* this should never happend unless the connection to the node or the node
           stops working *)
        Lwt.fail (Exit_watch [])
      | Some (Error e)  ->
        Verbose.error "error when tried to peak stream";
        Lwt.return (Error e)
      | Some Ok { Client_baking_blocks.level ; _ } ->
        let level = Alpha_context.Raw_level.to_int32 level in
        start_watch >>= fun last_treated_block ->
        (if level <> last_treated_block then
           bootstrap
             ?use_disk_cache
             ~conn ~cctxt ~first_alpha_level:last_treated_block
             ~up_to:level ~heights ()
         else
           Lwt.return (Ok ())) >>= function
        | Ok () ->
          Lwt_stream.fold_s fold_f stream (Ok ())
        | Error e ->
          Verbose.error "error while bootstrapping: %a" pp_print_error e;
          Lwt.return (Error e)
    end begin function
      | Exit_watch err ->
        Lwt.return (Error err)
      | exn ->
        fail (Exn exn)
    end
  end


let do_mempool_indexing pool cctxt =
  let prev = ref `_0 in
  let new_proto = ref true in
  let level = ref 0l in
  let one b _r = match b with
    | (Error e)  ->
      Verbose.error "# error when tried to peak stream: %a" pp_print_error e;
      Lwt.return (Error e)
    | (Ok { Client_baking_blocks.protocol ; next_protocol ; level = bl ; _ }) ->
      level := Alpha_context.Raw_level.to_int32 bl;
      begin
        if (protocol, next_protocol) = (hash_6, hash_6)
        && !level < mainnet_protocol_heights.proto_006
           (* assuming here that other supported networks will never
              reach mainnet's protocol transition 6 to 7 block anyways *)
        then
          if !prev = `_6 then
            return ()
          else
            begin
              prev := `_6;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.printf "# watching mempool proto 6";
                  Tezos_indexer_006_PsCARTHA.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_7, hash_7)
             || (protocol, next_protocol) = (hash_6, hash_7)
             || !level >= mainnet_protocol_heights.proto_006 then
          if !prev = `_7 then
            return ()
          else
            begin
              prev := `_7;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.printf "# watching mempool proto 7";
                  Tezos_indexer_007_PsDELPH1.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else
          Lwt.fail (Failure "unsupported protocols for monitoring mempool")
      end
  in
  Client_baking_blocks.info cctxt (`Head 0) >>= fun b ->
  one b () >>= fun _ ->
  Client_baking_blocks.monitor_valid_blocks
    cctxt ~next_protocols:None ()
  >>=? fun stream ->
  Lwt_stream.fold_s one stream (Ok ())


let update_balances no_balance_updates ?(limit=100) pool cctxt heights =
  let run conn =
    let f get_balanceless_contracts min_height max_height record_contract_balance =
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      Conn.collect_list
        get_balanceless_contracts
        (min_height, max_height, limit)
      >>= Db_shell.caqti_or_fail >>= fun to_be_filled ->
      let counter = ref 0 in
      Lwt_list.iter_s (fun (k, bh, level) ->
          incr counter;
          record_contract_balance conn cctxt ~bh ~level ~k
        ) to_be_filled >>= fun () ->
      if !counter > 0 && !counter < limit then
        Lwt.return 0
      else
        Lwt.return 1
    in
    let rec loop proto =
      match proto with
      | 1 ->
        f Tezos_indexer_001_PtCJ7pwo.Db_alpha.Contract_balance_table.get_balanceless_contracts
          2l heights.proto_001 Tezos_indexer_001_PtCJ7pwo.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 2 ->
        f Tezos_indexer_002_PsYLVpVv.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_001 heights.proto_002 Tezos_indexer_002_PsYLVpVv.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 3 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_002 heights.proto_003 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 4 ->
        f Tezos_indexer_004_Pt24m4xi.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_003 heights.proto_004 Tezos_indexer_004_Pt24m4xi.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 5 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_004 heights.proto_005 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 6 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_005 heights.proto_006 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 7 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_006 heights.proto_007 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= fun proto_offset ->
        loop (proto_offset + proto)
      | 8 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          heights.proto_007 heights.proto_008 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= (function
            | 1 -> loop 1
            | 0 -> loop 8
            | _ -> assert false)
      | _ -> assert false
    in
    loop 1
  in
  if no_balance_updates then
    return_unit
  else
    Caqti_lwt.Pool.use
      (fun conn ->
         run conn
      ) pool >>= Db_shell.caqti_or_fail >>= return


let index ?use_disk_cache
    first_alpha_level tezos_url tezos_client_dir db
    no_snapshot_blocks snapshot_blocks_only verbose debug
    rejected_blocks_depth
    keep_rejected
    mempool_indexing
    heights
    verbosity
    balance_updates_only
    no_balance_updates
    cache_blocks_from
    cache_blocks_upto
    cache_blocks
    cache_blocks_dir
  =
  Verbose.vlevel := verbosity;
  Verbose.verbose := verbose;
  Verbose.debug := debug;
  Caqti_lwt.connect_pool db |> Db_shell.caqti_or_fail >>= fun pool ->
  Tezos_indexer_lib.Db_shell.connect db >>= fun conn ->
  Lwt.async (fun () -> record_action pool >>= fun _ -> Lwt.return_unit);
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>= function
  | Error _ -> Lwt.fail (Failure "FIXME")
  | Ok (rpc_config, confirmations, host, port, tls) ->
  Verbose.printf "# host=%S ; port=%d ; tls=%b" host port tls;
  let cctxt =
    new Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full
      (Tezos_cfg.tezos_indexer_full ~rpc_config ?confirmations ()) in
  Verbose.Debug.printf "# Context created";
  begin if cache_blocks then
      let dir = cache_blocks_dir
      and from = cache_blocks_from
      and upto = cache_blocks_upto in
      Tezos_indexer_lib.File_blocks.write_blocks cctxt heights ~dir ~from ~upto
      >>= function
      | Ok _ ->
        print_endline __LOC__;
        exit 0
      | Error err ->
        Verbose.error "%a@." pp_print_error err;
        exit 22
    else
      return_unit
  end >>=? fun () ->
  let start_watch, push_start_watch = Lwt.wait () in
  let start_snapshots, push_start_snapshots = Lwt.wait () in
  if mempool_indexing then
    do_mempool_indexing pool cctxt
  else if snapshot_blocks_only then
    snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt
  else if balance_updates_only then
    update_balances no_balance_updates pool cctxt heights
  else
    let a = snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt
    and b = watch cctxt conn rejected_blocks_depth keep_rejected use_disk_cache first_alpha_level heights start_watch
    and c = bootstrap ?use_disk_cache ~conn ~cctxt ~first_alpha_level ~heights ~push_start_snapshots ~push_start_watch ()
    and d = update_balances no_balance_updates pool cctxt heights
    in
    a >>=? fun _ -> b >>=? fun _ -> c >>=? fun _ -> d


let index_prepare
    use_disk_cache
    disk_cache_dir
    first_alpha_level tezos_url tezos_client_dir db
    no_snapshot_blocks snapshot_blocks_only verbose debug
    print_db_schema
    version
    rejected_blocks_depth
    keep_rejected
    mempool_indexing
    proto_001 proto_002 proto_003 proto_004 proto_005 proto_006
    proto_007 proto_008
    mainnet
    carthagenet
    delphinet
    edonet
    ebetanet
    verbosity
    balance_updates_only
    no_balance_updates
    cache_blocks_from
    cache_blocks_upto
    cache_blocks
    cache_blocks_dir
    () =
  if ebetanet then begin
    prerr_endline "ebetanet is no longer supported: it was replaced by edonet. Please use v6.0.0 if you still need to index ebetanet.";
    exit 1
  end;
  if print_db_schema then begin
    Db_schema.print ();
    exit 0;
  end;
  if version then
    Version.version ();
  let heights =
    if mainnet then mainnet_protocol_heights
    else if carthagenet then carthagenet_protocol_heights
    else if delphinet then delphinet_protocol_heights
    else if edonet then edonet_protocol_heights
    else {
      proto_001; proto_002; proto_003; proto_004; proto_005; proto_006; proto_007; proto_008;
    }
  in
  index
    ?use_disk_cache:(if use_disk_cache then Some disk_cache_dir else None)
    first_alpha_level tezos_url tezos_client_dir db
    no_snapshot_blocks snapshot_blocks_only verbose debug
    rejected_blocks_depth
    keep_rejected
    mempool_indexing
    heights
    verbosity
    balance_updates_only
    no_balance_updates
    cache_blocks_from
    cache_blocks_upto
    cache_blocks
    cache_blocks_dir

let index_cmd_lwt =
  let open Cmdliner_helpers.Terms in
  let cache_blocks_from = Arg.(value & opt int32 2l &
                               info ["cache-blocks-from"]
                                 ~doc:"cache blocks from specified level") in
  let cache_blocks_upto = Arg.(value & opt int32 Int32.max_int &
                               info ["cache-blocks-upto"]
                                 ~doc:"cache blocks up to specified level") in
  let cache_blocks = Arg.(value & flag &
                          info ["do-cache-blocks"]
                            ~doc:"cache blocks") in
  let cache_blocks_dir = Arg.(value & opt string "./cache/" &
                              info ["cache-blocks-directory"]
                                ~doc:"write cache blocks in specified directory") in
  let no_balance_updates = Arg.(value & flag &
                                info ["no-contract-balances"]
                                  ~doc:"don't fill balances in table contract_balances") in
  let balance_updates_only = Arg.(value & flag &
                                  info ["contract-balances-only"]
                                    ~doc:"only fill balances in table contract_balances") in
  let use_disk_cache = Arg.(value & flag &
                            info ["use-disk-cache"]
                              ~doc:"use disk cache for blocks") in
  let disk_cache_dir = Arg.(value & opt string "." &
                            info ["disk-cache-dir"]
                              ~doc:"tell where the cache files are)") in
  let proto_001 = Arg.(value & opt int32 mainnet_protocol_heights.proto_001 &
                       info ["proto-001"]
                         ~doc:"height of last proto 001 block") in
  let proto_002 = Arg.(value & opt int32 mainnet_protocol_heights.proto_002 &
                       info ["proto-002"]
                         ~doc:"height of last proto 002 block") in
  let proto_003 = Arg.(value & opt int32 mainnet_protocol_heights.proto_003 &
                       info ["proto-003"]
                         ~doc:"height of last proto 003 block") in
  let proto_004 = Arg.(value & opt int32 mainnet_protocol_heights.proto_004 &
                       info ["proto-004"]
                         ~doc:"height of last proto 004 block") in
  let proto_005 = Arg.(value & opt int32 mainnet_protocol_heights.proto_005 &
                       info ["proto-005"]
                         ~doc:"height of last proto 005 block") in
  let proto_006 = Arg.(value & opt int32 mainnet_protocol_heights.proto_006 &
                       info ["proto-006"]
                         ~doc:"height of last proto 006 block") in
  let proto_007 = Arg.(value & opt int32 mainnet_protocol_heights.proto_007 &
                       info ["proto-007"]
                         ~doc:"height of last proto 007 block") in
  let proto_008 = Arg.(value & opt int32 mainnet_protocol_heights.proto_008 &
                       info ["proto-008"]
                         ~doc:"height of last proto 008 block") in
  let verbose_mode =
    Arg.(value & flag & info ~doc:"verbose mode" ["verbose"]) in
  let debug_mode =
    Arg.(value & flag & info ~doc:"debug mode" ["debug"]) in
  let snapshot_blocks_only =
    Arg.(value & flag & info ~doc:"get/update snapshot blocks ONLY (forever)" ["snapshots"]) in
  let no_snapshot_blocks =
    Arg.(value & flag & info ~doc:"deactivate snapshot blocks" ["no-snapshots"]) in
  let mempool_indexing =
    Arg.(value & flag & info ~doc:"index mempool ONLY: every other indexing activity will be disabled. You may run those in another process." ["mempool-only"]) in
  let rejected_blocks_depth =
    Arg.(value & opt int32 100l &
         info ["rejected-blocks-depth"]
           ~doc:"how far in the past to look for rejected blocks (happens at every new block during the watch process) \
                 - Default value is 100, and 0 means not to look at all (therefore they won't be detected nor marked)") in
  let keep_rejected =
    Arg.(value & flag & info ~doc:"keep rejected blocks (a.k.a. uncle blocks) and mark them as such, instead of automatically deleting them" ["keep-rejected-blocks"]) in
  let doc = "Follow Tezos blockchain and register new blocks." in
  let notes = "WARNING The behavior is undefined if you use several incompatible options.
For instance, you shouldn't use --proto-XXX and --nameofnetwork at the same time.
You shouldn't use both --mainnet and --carthagenet at the same time either.
Last but not least, if the indexer connects to a node for which protocol heights don't match during bootstrap, it won't work.
Enjoy!" in
  let carthagenet =
    Arg.(value & flag & info ~doc:"set all proto-XXX values for carthagenet" ["carthagenet"]) in
  let mainnet =
    Arg.(value & flag & info ~doc:"set all proto-XXX values for mainnet" ["mainnet"]) in
  let delphinet =
    Arg.(value & flag & info ~doc:"set all proto-XXX values for delphinet" ["delphinet"]) in
  let ebetanet =
    Arg.(value & flag & info ~doc:"ebetanet is no longer supported! Please use v6.0.0 if you need it" ["ebetanet"]) in
  let edonet =
    Arg.(value & flag & info ~doc:"set all proto-XXX values for edonet" ["edonet"]) in
  let verbosity = Arg.(value & opt int 5 &
                       info ["verbosity"]
                         ~doc:"verbosity level (requires --verbose, otherwise will have no effect). \
                               0: some logs. 1: block level. 2: operation level. \
                               3: sub-operation level. 4: more logs. 5: even more logs!") in
  let version = Arg.(value & flag & info ~doc:"print version and exit" ["version"]) in
  let print_db_schema = Arg.(value & flag & info ~doc:"print default full DB schema and exit" ["db-schema"]) in
  Term.(const index_prepare $
        use_disk_cache $
        disk_cache_dir $
        first_block_level $
        uri_option ~args:["tezos-url"] ~doc:"URL of a running Tezos node" () $
        tezos_client_dir $
        db ~default:default_db_url $
        no_snapshot_blocks $
        snapshot_blocks_only $
        verbose_mode $
        debug_mode $
        print_db_schema $
        version $
        rejected_blocks_depth $
        keep_rejected $
        mempool_indexing $
        proto_001 $
        proto_002 $
        proto_003 $
        proto_004 $
        proto_005 $
        proto_006 $
        proto_007 $
        proto_008 $
        mainnet $
        carthagenet $
        delphinet $
        edonet $
        ebetanet $
        verbosity $
        balance_updates_only $
        no_balance_updates $
        cache_blocks_from $
        cache_blocks_upto $
        cache_blocks $
        cache_blocks_dir $
        const ()),
  Term.info ~doc ~sdocs:notes
    "tezos-indexer"


let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Verbose.error "Error: %a" pp_exn exn;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Verbose.error "%a@." pp_print_error err;
    exit Verbose.ExitCodes.other
  | Ok () ->
    ()


let cmd_of_lwt (term, info) =
  Term.((const lwt_run) $ term), info

let () = match Term.eval (cmd_of_lwt index_cmd_lwt) with
  | `Error _ -> exit Verbose.ExitCodes.cmdline
  | #Term.result -> exit Verbose.ExitCodes.ok
