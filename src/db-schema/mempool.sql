-- Open Source License
-- Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for operations in the mempool, so you may track the life of an operation

CREATE TYPE mempool_op_status AS ENUM ('applied', 'refused', 'branch_refused', 'unprocessed', 'branch_delayed');

CREATE TABLE mempool_op_branch (
  hash char(51) not null,
  branch char(51) not null,
  primary key(hash)
);

CREATE TABLE mempool_operation_alpha (
  hash char(51) not null,
  first_seen_level int not null,
  first_seen_timestamp double precision not null,
  last_seen_level int not null,
  last_seen_timestamp double precision not null,
  status mempool_op_status not null,
  id smallint not null,
  -- index of op in contents_list
  operation_kind smallint not null,
  -- 0: Endorsement
  -- 1: Seed_nonce_revelation
  -- 2: double_endorsement_evidence
  -- 3: Double_baking_evidence
  -- 4: Activate_account
  -- 5: Proposals
  -- 6: Ballot
  -- 7: Manager_operation { operation = Reveal _ ; _ }
  -- 8: Manager_operation { operation = Transaction _ ; _ }
  -- 9: Manager_operation { operation = Origination _ ; _ }
  -- 10: Manager_operation { operation = Delegation _ ; _ }
  source char(36),
  -- sender
  destination char(36),
  -- receiver, if any
  operation_alpha jsonb,
  autoid SERIAL, -- this field should always be last
  primary key(hash, id, status),
  foreign key(source) references addresses(address),
  foreign key(destination) references addresses(address),
  foreign key(hash) references mempool_op_branch(hash)
);
create index mempool_operations_hash on mempool_operation_alpha(hash);
create index mempool_operations_status on mempool_operation_alpha(status); --OPT
create index mempool_operations_source on mempool_operation_alpha(source);
create index mempool_operations_destination on mempool_operation_alpha(destination);
create index mempool_operations_kind on mempool_operation_alpha(operation_kind);


-- drop function insert_into_mempool_operation_alpha;
create or replace function insert_into_mempool_operation_alpha (
  xbranch char(51),
  xlevel int,
  xhash char(51),
  xstatus mempool_op_status,
  xid smallint,
  xoperation_kind smallint,
  xsource char(36),
  xdestination char(36),
  xseen_timestamp double precision,
  xoperation_alpha jsonb
)
returns void
as $$
  insert into addresses(address) select xsource where xsource is not null on conflict do nothing;
  insert into addresses(address) select xdestination where xdestination is not null on conflict do nothing;
  insert into mempool_op_branch (hash, branch) values (xhash, xbranch) on conflict do nothing;
  insert into mempool_operation_alpha (
      hash
    , first_seen_level
    , first_seen_timestamp
    , last_seen_level
    , last_seen_timestamp
    , status
    , id
    , operation_kind
    , source
    , destination
    , operation_alpha
  ) values (
      xhash
    , xlevel
    , xseen_timestamp
    , xlevel
    , xseen_timestamp
    , xstatus
    , xid
    , xoperation_kind
    , xsource
    , xdestination
    , xoperation_alpha
  ) on conflict (hash, id, status)
  do update set
    last_seen_level = xlevel
  , last_seen_timestamp = xseen_timestamp
  where mempool_operation_alpha.hash = xhash
    and mempool_operation_alpha.id = xid
    and mempool_operation_alpha.status = xstatus
$$ language sql;


create or replace view light_mempool_operations as
select
  hash,
  first_seen_level,
  last_seen_level,
  (last_seen_timestamp-first_seen_timestamp) as presence,
  status,
  id,
  operation_kind,
  source,
  destination,
  autoid
from mempool_operation_alpha;
