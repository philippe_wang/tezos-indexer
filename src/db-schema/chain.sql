-- Open Source License
-- Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>
-- Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

select 'creating table indexer_log' as action;
create table indexer_log (
   timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   version text not null,
   argv text not null,
   action text
);


select 'creating table chain' as action;
create table chain (
  hash char(15) primary key
  -- chain id
);

-- create table block_hash (
--   hash char(51) primary key
-- );


-- this table inlines blocks and block headers
-- see lib_base/block_header.mli
select 'creating table block' as action;
create table block (
  hash char(51) unique, -- not null since part of primary key
  -- Block hash.
  -- 51 = 32 bytes hashes encoded in b58check + length of prefix "B"
  -- see lib_crypto/base58.ml
  level int, -- not null since part of primary key
  -- Height of the block, from the genesis block.
  proto int not null,
  -- Number of protocol changes since genesis modulo 256.
  predecessor char(51) not null,
  -- Hash of the preceding block.
  timestamp timestamp not null,
  -- Timestamp at which the block is claimed to have been created.
  validation_passes smallint not null,
  -- Number of validation passes (also number of lists of operations).
  merkle_root char(53) not null,
  -- see [operations_hash]
  -- Hash of the list of lists (actually root hashes of merkle trees)
  -- of operations included in the block. There is one list of
  -- operations per validation pass.
  -- 53 = 32 bytes hashes encoded in b58 check + "LLo" prefix
  fitness varchar(64) not null,
  -- A sequence of sequences of unsigned bytes, ordered by length and
  -- then lexicographically. It represents the claimed fitness of the
  -- chain ending in this block.
  context_hash char(52) not null,
  -- Hash of the state of the context after application of this block.
  rejected bool not null default false, -- AKA "uncle block" or "forked"
  -- if true, this block is not in the blockchain anymore
  -- if false, we're not sure! It might be rejected later...
  primary key (hash, level)
  -- , foreign key (hash) references block_hash(hash)
  -- , foreign key (predecessor) references block_hash(hash)
);
-- alter table block add column rejected bool;
create index block_rejected on block using btree (rejected); --BOOTSTRAPPED
create index block_level on block using btree (level); --BOOTSTRAPPED
create index block_hash on block using btree (hash);



-- Record extracted from
-- Alpha_block_services.operation
select 'creating table operation' as action;
create table operation (
  -- Note: an operation may point to a rejected block only if the
  -- operation itself was deleted from the chain.
  -- If the operation was included in a rejected block but then
  -- reinjected into another block, then this table contains the
  -- latest block_hash associated to that operation.
  -- Hypothesis: the latest write is always right.
  hash char(51) not null, -- operation hash
  chain char(15) not null, -- chain id (irrelevant until other chains are indexed)
  block_hash char(51) not null, -- block hash
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  primary key (hash, block_hash),
  foreign key (chain) references chain (hash), -- link to [chain] table
  foreign key (block_hash) references block(hash) -- link to [block] table
);
--OPT create index operation_chain on operation using btree (chain);
create index operation_block on operation using btree (block_hash);
create index operation_hash on operation using btree (hash);
create index operation_autoid on operation using btree (autoid); --OPT --BOOTSTRAPPED

-- BUG FIX
-- alter table operation drop constraint operation_pkey cascade;
-- alter table operation add constraint operation_pkey PRIMARY KEY(hash, block_hash);


-- protocol-specific content of an operation
select 'creating table operation_alpha' as action;
create table operation_alpha (
  hash char(51) not null,
  -- operation hash
  id smallint not null,
  -- index of op in contents_list
  operation_kind smallint not null,
  -- from mezos/chain_db.ml
  -- see proto_alpha/operation_repr.ml
  -- (this would better be called "kind")
  -- type of operation alpha
  -- 0: Endorsement
  -- 1: Seed_nonce_revelation
  -- 2: double_endorsement_evidence
  -- 3: Double_baking_evidence
  -- 4: Activate_account
  -- 5: Proposals
  -- 6: Ballot
  -- 7: Manager_operation { operation = Reveal _ ; _ }
  -- 8: Manager_operation { operation = Transaction _ ; _ }
  -- 9: Manager_operation { operation = Origination _ ; _ }
  -- 10: Manager_operation { operation = Delegation _ ; _ }
  sender char(36),
  -- sender, if any
  receiver char(36),
  -- receiver, if any
  autoid SERIAL UNIQUE, -- this field should always be last
  primary key (hash, id),
  -- foreign key (hash) references operation(hash), --FIXME
  foreign key (sender) references addresses(address),
  foreign key (receiver) references addresses(address)
);
create index operation_alpha_kind on operation_alpha using btree (operation_kind); --BOOTSTRAPPED
create index operation_alpha_hash on operation_alpha using btree (hash);
create index operation_alpha_autoid on operation_alpha using btree (autoid); --OPT --BOOTSTRAPPED
create index operation_alpha_sender on operation_alpha using btree (sender); --BOOTSTRAPPED
create index operation_alpha_receiver on operation_alpha using btree (receiver); --BOOTSTRAPPED

select 'creating table manager_numbers' as action;
create table manager_numbers (
  hash char(51) not null,
  -- operation hash
  id smallint not null,
  -- operation id
  counter numeric,
  -- counter
  gas_limit numeric,
  -- gas limit
  storage_limit numeric,
  -- storage limit
  primary key (hash, id),
  foreign key (hash, id) references operation_alpha(hash, id)
);


-- implicit accounts(including deactivated ones)
select 'creating table implicit' as action;
create table implicit (
  pkh char(36) primary key,
  -- b58-encoded public key hash: tz1/tz1/tz3...
  activated char(51),
  -- hash of operation at which activation was performed
  -- (see mezos/chain_db.ml/upsert_activated)
  revealed char(51),
  -- hash of operation at which revelation was performed
  -- (see mezos/chain_db.ml/upsert_activated)
  pk varchar(55),
  -- Full public key (optional)
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
--  foreign key (activated) references operation(hash), --FIXME
--  foreign key (revealed)  references operation(hash), --FIXME
  foreign key (pkh) references addresses(address)
);
create index implicit_activated on implicit using btree (activated); --BOOTSTRAPPED
create index implicit_revealed on implicit using btree (revealed); --BOOTSTRAPPED
create index implicit_pkh on implicit using btree (pkh); --BOOTSTRAPPED
create index implicit_autoid on implicit using btree (autoid); --OPT --BOOTSTRAPPED

-- endorsements
select 'creating table endorsement' as action;
create table endorsement (
  -- block_hash char(51), -- this was the hash of the block from which the information comes
  operation_hash char(51),
  op_id smallint,
  level int,
  delegate char(36),
  slots smallint[],
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  primary key (operation_hash, op_id, level, delegate, slots),
--  primary key (block_hash, operation_hash, op_id, level, delegate, slots),
--  foreign key (block_hash) references block(hash),
--  foreign key (operation_hash)  references operation(hash), --FIXME
  foreign key (operation_hash, op_id)  references operation_alpha(hash, id), --OPT
  foreign key (delegate) references implicit(pkh)
);
create index endorsement_op on endorsement using btree (operation_hash);
-- create index endorsement_block_hash on endorsement using btree (block_hash);
create index endorsement_autoid on endorsement using btree (autoid); --OPT --BOOTSTRAPPED

select 'creating table seed_nonce_revelation' as action;
create table seed_nonce_revelation (
  block_hash char(51),
  operation_hash char(51),
  -- operation hash
  op_id smallint not null,
  -- index of the operation in the block's list of operations
  level int not null,
  nonce char(66) not null,
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  primary key (block_hash, operation_hash, op_id),
  foreign key (block_hash) references block(hash)
  , foreign key (operation_hash, op_id) references operation_alpha(hash, id) --OPT
--  ,foreign key (operation_hash) references operation(hash) --FIXME
);
create index seed_nonce_revelation_op on seed_nonce_revelation using btree (operation_hash);
create index seed_nonce_revelation_block_hash on seed_nonce_revelation using btree (block_hash);
create index seed_nonce_revelation_autoid on seed_nonce_revelation using btree (autoid); --OPT --BOOTSTRAPPED

/* Block info is obtained through
   lib_shell_services/Make()()/info
   instantiated in
   proto_alpha/lib_client/proto_alpha.ml
} */

-- Block table.
-- from the doc:
-- "level_position = cycle * blocks_per_cycle + cycle_position"
select 'creating table block_alpha' as action;
create table block_alpha (
  hash char(51) primary key
  -- block hash
  , baker char(36) not null
  -- pkh of baker
  , level_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The level of the block relative to the block that
     starts protocol alpha. This is specific to the
     protocol alpha. Other protocols might or might not
     include a similar notion.
  */
  , cycle int not null
  -- cycle
  , cycle_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The current level of the block relative to the first
     block of the current cycle.
  */
  , voting_period jsonb not null
  /* increasing integer.
     from proto_alpha/level_repr:
     voting_period = level_position / blocks_per_voting_period */
  , voting_period_position int not null
  -- voting_period_position = remainder(level_position / blocks_per_voting_period)
  , voting_period_kind smallint not null
  /* Proposal = 0
     Testing_vote = 1
     Testing = 2
     Promotion_vote = 3
     Adoption = 4
   */
  , consumed_milligas numeric not null
  /* total milligas consumed by block. Arbitrary-precision integer. */
  , foreign key (hash) references block(hash)
  -- , foreign key (baker) references implicit(pkh) -- pre-babylon
  , foreign key (baker) references addresses(address)
);
create index block_alpha_baker on block_alpha using btree (baker); --BOOTSTRAPPED
create index block_alpha_level_position on block_alpha using btree (level_position); --BOOTSTRAPPED
create index block_alpha_cycle on block_alpha using btree (cycle); --BOOTSTRAPPED
create index block_alpha_cycle_position on block_alpha using btree (cycle_position); --BOOTSTRAPPED
create index block_alpha_hash on block_alpha using btree (hash);


-- deactivated accounts
select 'creating table deactivated' as action;
create table deactivated (
  pkh char(36) not null,
  -- pkh of the deactivated account(tz1...)
  block_hash char(51) not null,
  -- block hash at which deactivation occured
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  primary key (pkh, block_hash),
  foreign key (pkh) references implicit(pkh),
  foreign key (block_hash) references block(hash)
);
create index deactivated_pkh on deactivated using btree (pkh);
create index deactivated_block_hash on deactivated using btree (block_hash);
create index deactivated_autoid on deactivated using btree (autoid); --OPT --BOOTSTRAPPED

-- contract(implicit:tz1... or originated:KT1...) table
-- two ways of updating this table:
-- - on bootstrap, scanning preexisting contracts
-- - when scanning ops, looking at an origination/revelation
select 'creating table contract' as action;
create table contract (
  address char(36) not null,
  -- contract address, b58check format
  block_hash char(51) not null,
  -- block hash
  mgr char(36),
  -- manager
  delegate char(36),
  -- delegate
  spendable bool,
  -- spendable flag -- obsolete since proto 5
  delegatable bool,
  -- delegatable flag, soon obsolete?
  credit bigint,
  -- credit
  preorig char(36),
  -- comment from proto_alpha/apply:
  -- The preorigination field is only used to early return
  -- the address of an originated contract in Michelson.
  -- It cannot come from the outside.
  script jsonb,
  -- Json-encoded Micheline script
  autoid SERIAL UNIQUE, -- this field should always be last -- not optional since addresses are no longer unique
  primary key (address, block_hash),
  foreign key (block_hash) references block(hash),
  foreign key (mgr) references implicit(pkh),
  --  foreign key (delegate) references implicit(pkh), -- pre-babylon
  foreign key (preorig) references addresses(address),
  foreign key (address) references addresses(address),
  foreign key (mgr) references addresses(address),
  foreign key (delegate) references addresses(address)
);
create index contract_block on contract using btree (block_hash); --BOOTSTRAPPED
create index contract_mgr on contract using btree (mgr); --BOOTSTRAPPED
create index contract_delegate on contract using btree (delegate); --BOOTSTRAPPED
create index contract_preorig on contract using btree (preorig); --BOOTSTRAPPED
create index contract_address on contract using btree (address);
create index contract_autoid on contract using btree (autoid); --BOOTSTRAPPED


-- The following view, contract_legacy, is deactivated by default because it's too slow!
-- This view was to provide compatibility with the old version of the contract table, where addresses where unique
--OPT select 'creating view contract_legacy' as action;
--OPT create or replace view contract_legacy as
--OPT select
--OPT distinct c.address,
--OPT  c.block_hash,
--OPT  coalesce(c.mgr, (select mgr from contract c2 where c2.mgr is not null and c2.address = c.address order by c2.autoid desc limit 1)) as mgr,
--OPT  coalesce(c.delegate, (select delegate from contract c2 where c2.delegate is not null and c2.address = c.address order by c2.autoid desc limit 1)) as delegate,
--OPT  coalesce(c.spendable, (select spendable from contract c2 where c2.spendable is not null and c2.address = c.address order by c2.autoid desc limit 1)) as spendable,
--OPT  coalesce(c.delegatable, (select delegatable from contract c2 where c2.delegatable is not null and c2.address = c.address order by c2.autoid desc limit 1)) as delegatable,
--OPT  coalesce(c.credit, (select credit from contract c2 where c2.credit is not null and c2.address = c.address order by c2.autoid desc limit 1)) as credit,
--OPT  coalesce(c.preorig, (select preorig from contract c2 where c2.preorig is not null and c2.address = c.address order by c2.autoid desc limit 1)) as preorig,
--OPT  coalesce(c.script, (select script from contract c2 where c2.script is not null and c2.address = c.address order by c2.autoid desc limit 1)) as script,
--OPT c.autoid from
--OPT contract c, block b
--OPT where c.block_hash = b.hash and not b.rejected
--OPT order by autoid desc;

select 'creating table contract_balance' as action;
create table contract_balance (
  address char(36) not null,
  block_hash char(51) not null,
  balance bigint, -- make it nullable so that it can be filled asynchronously
  block_level bigint not null, -- this field is only meant to speed up searches
  primary key (address, block_hash),
  -- N.B. it would be bad to have "address" as a primary key,
  -- because if you update a contract's balance using a
  -- rejected block(uncle block) and then the new balance is not updated
  -- once the rejected block is discovered,
  -- you end up with wrong information
  foreign key (address) references addresses(address),
  foreign key (block_hash, block_level) references block(hash, level)
);
create index contract_balance_block_level on contract_balance using btree (block_level); --BOOTSTRAPPED
create index contract_balance_address on contract_balance using btree (address);
create index contract_balance_block_hash on contract_balance using btree (block_hash); --BOOTSTRAPPED


select 'creating table tx' as action;
-- transaction table
create table tx (
  operation_hash char(51) not null,
  -- operation hash(starts with "o", see lib_crypto/base58)
  op_id smallint not null,
  -- index of the operation in the block's list of operations
  source char(36) not null,
  -- source address
  destination char(36) not null,
  -- dest address
  fee bigint not null,
  -- fees
  amount bigint not null,
  -- amount
  parameters text,
  -- optional parameters to contract in json-encoded Micheline
  storage jsonb,
  -- optional parameter for storage update
  consumed_milligas numeric not null,
  -- consumed milligas
  storage_size numeric not null,
  -- storage size
  paid_storage_size_diff numeric not null,
  -- paid storage size diff
  entrypoint text,
  -- entrypoint
  bigmapdiff jsonb,
  -- bigmap diff
  autoid SERIAL UNIQUE, -- this field should always be last
  primary key (operation_hash, op_id),
  foreign key (operation_hash, op_id) references operation_alpha(hash, id),
-- foreign key (operation_hash) references operation(hash), --FIXME
  foreign key (source) references addresses(address),
  foreign key (destination) references addresses(address)
);
create index tx_source on tx using btree (source); --BOOTSTRAPPED
create index tx_destination on tx using btree (destination); --BOOTSTRAPPED
create index tx_operation_hash on tx using btree (operation_hash);
create index tx_autoid on tx using btree (autoid); --OPT --BOOTSTRAPPED

-- create table origination_results (
--   operation_hash char(51) not null,
--   op_id smallint not null,
--   consumed_milligas numeric not null,
--   -- consumed milligas
--   storage_size numeric not null,
--   -- storage size
--   paid_storage_size_diff numeric not null,
--   -- paid storage size diff
--   bigmapdiff jsonb,
--   -- bigmap diff
--   ...
-- );

-- origination table
select 'creating table origination' as action;
create table origination (
  operation_hash char(51) not null,
  -- operation hash
  op_id smallint not null,
  -- index of the operation in the block's list of operations
  source char(36) not null,
  -- source of origination op
  k char(36) not null,
  -- address of originated contract
  consumed_milligas numeric not null,
  -- consumed milligas
  storage_size numeric not null,
  -- storage size
  paid_storage_size_diff numeric not null,
  -- paid storage size diff
  bigmapdiff jsonb,
  -- bigmap diff
  fee bigint not null,
  -- fees
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  primary key (operation_hash, op_id),
  foreign key (operation_hash, op_id) references operation_alpha(hash, id),
-- foreign key (operation_hash) references operation(hash), --FIXME
  foreign key (source) references addresses(address),
  foreign key (k) references addresses(address)
);
create index origination_source on origination using btree (source); --BOOTSTRAPPED
create index origination_k on origination using btree (k); --BOOTSTRAPPED
create index origination_operation_hash on origination using btree (operation_hash);
create index origination_autoid on origination using btree (autoid); --OPT --BOOTSTRAPPED

select 'creating table delegation' as action;
create table delegation (
  operation_hash char(51) not null
  -- operation hash
  , op_id smallint not null
  -- index of the operation in the block's list of operations
  , source char(36) not null
  -- source of the delegation op
  , pkh char(36)
  -- optional delegate
  , consumed_milligas numeric -- nullable because of proto 1 & 2
  -- consumed milligas
  , fee bigint not null
  -- fees
  , autoid SERIAL UNIQUE --OPT
  , primary key (operation_hash, op_id)
  , foreign key (operation_hash, op_id) references operation_alpha(hash, id)
-- , foreign key (operation_hash) references operation(hash) --FIXME
  , foreign key (source) references addresses(address)
  , foreign key (pkh) references addresses(address)
  --  , foreign key (pkh) references implicit(pkh)
);
create index delegation_source on delegation using btree (source); --BOOTSTRAPPED
create index delegation_pkh on delegation using btree (pkh); --BOOTSTRAPPED
create index delegation_operation_hash on delegation using btree (operation_hash);
create index delegation_autoid on delegation using btree (autoid); --OPT --BOOTSTRAPPED


select 'creating table reveal' as action;
create table reveal (
  operation_hash char(51) not null
  -- operation hash
  , op_id smallint not null
  -- index of the operation in the block's list of operations
  , source char(36) not null
  -- source
  , pk char(55) not null
  -- revealed pk
  , consumed_milligas numeric -- nullable because of proto 1 & 2
  -- consumed milligas
  , fee bigint not null
  -- fees
  , autoid SERIAL UNIQUE --OPT
  , primary key (operation_hash, op_id)
  , foreign key (operation_hash, op_id) references operation_alpha(hash, id)
  , foreign key (source) references addresses(address)
);
create index reveal_source on reveal using btree (source); --BOOTSTRAPPED
create index reveal_pkh on reveal using btree (pk); --BOOTSTRAPPED
create index reveal_operation_hash on reveal using btree (operation_hash);
create index reveal_autoid on reveal using btree (autoid); --OPT --BOOTSTRAPPED


select 'creating table balance' as action;
create table balance (
  block_hash char(51) not null,
  -- block hash
  operation_hash char(51),
  -- operation hash
  op_id smallint,
  -- index of the operation in the blocks list of operations
  balance_kind smallint not null,
  -- balance kind:
  -- 0 : Contract
  -- 1 : Rewards
  -- 2 : Fees
  -- 3 : Deposits
  -- see proto_alpha/delegate_storage.ml/balance
  contract_address char(36) not null,
  -- b58check encoded address of contract(either implicit or originated)
  cycle int,
  -- cycle
  diff bigint not null,
  -- balance update
  -- credited if positve
  -- debited if negative
  autoid SERIAL UNIQUE, -- this field should always be last --OPT
  foreign key (block_hash) references block(hash),
  foreign key (operation_hash, op_id) references operation_alpha(hash, id),
-- foreign key (operation_hash) references operation(hash), --FIXME
  foreign key (contract_address) references addresses(address)
);
create index balance_block  on balance using btree (block_hash); --BOOTSTRAPPED
create index balance_op     on balance using btree (operation_hash, op_id);
create index balance_ophash on balance using btree (operation_hash); --BOOTSTRAPPED
create index balance_cat    on balance using btree (balance_kind); --BOOTSTRAPPED
create index balance_k      on balance using btree (contract_address); --BOOTSTRAPPED
create index balance_cycle  on balance using btree (cycle); --BOOTSTRAPPED
create index balance_autoid on balance using btree (autoid); --OPT --BOOTSTRAPPED

-- snapshots
-- the snapshot block for a given cycle is obtained as follows
-- at the last block of cycle n, the snapshot block for cycle n+6 is selected
-- Use [Storage.Roll.Snapshot_for_cycle.get ctxt cycle] in proto_alpha to
-- obtain this value.
-- RPC: /chains/main/blocks/${block}/context/raw/json/cycle/${cycle}
-- where:
-- ${block} denotes a block(either by hash or level)
-- ${cycle} denotes a cycle which must be in [cycle_of(level)-5,cycle_of(level)+7]
select 'creating table snapshot' as action;
create table snapshot (
  cycle int,
  level int,
  primary key (cycle, level)
);

-- Could be useful for baking.
-- create table delegate (
--   cycle int not null,
--   level int not null,
--   pkh char(36) not null,
--   balance bigint not null,
--   frozen_balance bigint not null,
--   staking_balance bigint not null,
--   delegated_balance bigint not null,
--   deactivated bool not null,
--   grace smallint not null,
--   primary key (cycle, pkh),
--   foreign key (cycle, level) references snapshot(cycle, level),
--   foreign key (pkh) references implicit(pkh)
-- );

-- Delegated contract table
-- It seems this table is not filled by Mezos yet
select 'creating table delegated_contract' as action;
create table delegated_contract (
  delegate char(36),
  -- tz1 of the delegate
  delegator char(36),
  -- address of the delegator (for now, KT1 but this could change)
  cycle int,
  level int,
  primary key (delegate, delegator, cycle, level),
  foreign key (delegate) references implicit(pkh),
  foreign key (delegator) references addresses(address),
  foreign key (cycle, level) references snapshot(cycle, level)
);
create index delegated_contract_cycle on delegated_contract using btree (cycle); --BOOTSTRAPPED
create index delegated_contract_level on delegated_contract using btree (level); --BOOTSTRAPPED
create index delegated_contract_delegate on delegated_contract using btree (delegate);
create index delegated_contract_delegator on delegated_contract using btree (delegator);

-- Could be useful for baking.
-- create table stake (
--   delegate char(36) not null,
--   level int not null,
--   k char(36) not null,
--   kind smallint not null,
--   diff bigint not null,
--   primary key (delegate, level, k, kind, diff),
--   foreign key (delegate) references implicit(pkh),
--   foreign key (k) references addresses(address)
-- );

-- VIEWS
select 'creating view level' as action;
create or replace view level as
  select
   level,
   hash
  from block
  order by level asc;

-- --  tx_full view
-- create or replace view tx_full as
--   select
--    operation_hash, -- operation hash
--    op_id, -- index in list of operations
--    b.hash as block_hash, -- block hash
--    b.level as level, -- block level
--    b.timestamp as timestamp, -- timestamp
--    source, -- source
--    k1.mgr as source_mgr, -- manager of source
--    destination, -- destination
--    k2.mgr as destination_mgr, -- manager of destination
--    fee, -- fees
--    amount, -- amount transfered
--    parameters, -- parameters to target contract, if any
--    storage, -- storage(cf. table tx)
--    entrypoint -- entrypoint(cf. table tx)
--   from tx
--   join operation on tx.operation_hash = operation.hash
--   join block b on operation.block_hash = b.hash and not b.rejected
--   join contract k1 on tx.source = k1.address
--   join contract k2 on tx.destination = k2.address;

select 'creating table balance_full' as action;
create or replace view balance_full as
  select
   block.level, -- block level
   block_alpha.cycle, -- cycle
   cycle_position, -- position in cycle
   operation_hash, -- operation hash
   op_id, -- index in list of operations
   contract_address, -- address of contract
   balance_kind, -- balance kind
   diff -- balance update
  from block
  natural join block_alpha
  join balance on block.hash = balance.block_hash;



-- DROP FUNCTION insert_or_update_contract();
select 'creating function insert_or_update_contract' as action;
CREATE OR REPLACE FUNCTION insert_or_update_contract (
  a char(36),
  b char(51),
  m char(36),
  d char(36),
  s bool,
  de bool,
  c bigint,
  p char(36),
  sc jsonb)
RETURNS void
AS $$
insert into contract (address, block_hash, mgr, delegate, spendable, delegatable, credit, preorig, script) values (a, b, m, d, s, de, c, p, sc) on conflict ON CONSTRAINT contract_pkey do update
set
mgr = m,
delegate = d,
spendable = s,
delegatable = de,
credit = c,
preorig = p,
script = sc
where contract.address = a and contract.block_hash = b
$$ LANGUAGE SQL;

-- DROP FUNCTION insert_or_update_tx();
select 'creating function insert_or_update_tx' as action;
CREATE OR REPLACE FUNCTION insert_or_update_tx (
  a char(51),
  b smallint,
  c char(36),
  d char(36),
  e bigint,
  f bigint,
  g text,
  h jsonb,
  i numeric,
  j numeric,
  k numeric,
  ep char, -- entrypoint
  l jsonb)
RETURNS void
AS $$
insert into tx values (a, b, c, d, e, f, g, h, i, j, k, ep, l) on conflict ON CONSTRAINT tx_pkey do update
set
 operation_hash = a,
 op_id = b,
 source = c,
 destination = d,
 fee = e,
 amount = f,
 parameters = g,
 storage = h,
 consumed_milligas = i,
 storage_size = j,
 paid_storage_size_diff = k,
 entrypoint = e,
 bigmapdiff = l
where tx.operation_hash = a and tx.op_id = b
$$ LANGUAGE SQL;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- VIEWS TO DEAL WITH REJECTED BLOCKS (AKA UNCLE BLOCKS)
--------------------------------------------------------------------------------

-- drop view block_view cascade;
select 'creating view block_view' as action;
create or replace view block_view as
select hash, level, proto, predecessor, timestamp, validation_passes, merkle_root, fitness, context_hash
from block b where not b.rejected;

select 'creating function mark_rejected_blocks' as action;
create or replace function mark_rejected_blocks(howfar int) returns table(hash char, level integer) as $$
update block b
set rejected = true
where not b.rejected
and b.level < (select max(level) from block)
and b.level > ((select max(level) from block) - howfar)
and (select count(*) from block x where x.level > b.level and x.predecessor = b.hash and not x.rejected) = 0
returning b.hash, b.level
$$ language sql;

select 'creating view operation_view' as action;
create or replace view operation_view as
select * from operation o where o.block_hash in (select hash from block_view);

select 'creating view operation_alpha_view' as action;
create or replace view operation_alpha_view as
select * from operation_alpha o where o.hash in (select hash from operation_view);

select 'creating view manager_numbers_view' as action;
create or replace view manager_numbers_view as
select * from manager_numbers where hash in (select hash from operation_view);

select 'creating view implicit_view' as action;
create or replace view implicit_view as
select * from implicit i where i.activated in (select hash from operation_view) and i.revealed in (select hash from operation_view);

select 'creating view block_alpha_view' as action;
create or replace view block_alpha_view as
select * from block_alpha b where b.hash in (select hash from block_view);

select 'creating view contract_view' as action;
create or replace view contract_view as
select * from contract c where c.block_hash in (select hash from block_view);

select 'creating view tx_view' as action;
create or replace view tx_view as
select * from tx where tx.operation_hash in (select hash from operation_view);

select 'creating view origination_view' as action;
create or replace view origination_view as
select * from origination o where o.operation_hash in (select hash from operation_view);

select 'creating view delegation_view' as action;
create or replace view delegation_view as
select * from delegation o where o.operation_hash in (select hash from operation_view);

select 'creating view balance_view' as action;
create or replace view balance_view as
select * from balance o where o.block_hash in (select hash from block_view);

select 'creating function balance_at_level' as action;
CREATE OR REPLACE FUNCTION balance_at_level(x varchar, lev int)
RETURNS TABLE(bal bigint)
AS $$
select coalesce(
  (SELECT c.balance
   FROM contract_balance c, block b
   WHERE c.address = x
   and c.block_level <= lev
   and c.block_hash = b.hash
--OPT    and not b.rejected
   order by c.block_level desc limit 1
  ),
  0) as bal
$$ LANGUAGE SQL;

-- SELECT balance_at_level('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 1000000);

select 'creating function would_be_orphan_operations' as action;
create or replace function would_be_orphan_operations (xblock_hash varchar)
-- returns operation_hash for operations that would no longer be attached to any block if block.hash = xblock_hash was deleted
returns table(operation_hash char)
as $$
with candidates  as (
select o.hash as hash, (select count(*) from operation ooo where ooo.hash = o.hash) as c
from operation o
where
o.hash in
 (select oo.hash from operation oo where oo.block_hash = xblock_hash)
)
select candidates.hash from candidates
where candidates.c = 1
;
$$ language sql;

select 'creating function delete_one_operation' as action;
create or replace function delete_one_operation (xoperation_hash varchar)
returns  void
as $$
delete from manager_numbers       where hash = xoperation_hash;
delete from endorsement           where operation_hash = xoperation_hash;
delete from tx                    where operation_hash = xoperation_hash;
delete from origination           where operation_hash = xoperation_hash;
delete from delegation            where operation_hash = xoperation_hash;
delete from reveal                where operation_hash = xoperation_hash;
delete from balance               where operation_hash = xoperation_hash;
delete from seed_nonce_revelation where operation_hash = xoperation_hash;
delete from balance               where operation_hash = xoperation_hash;
delete from operation_alpha       where hash = xoperation_hash;
update implicit i set activated = null where activated = xoperation_hash;
update implicit i set revealed = null where revealed = xoperation_hash;

$$ language sql;

select 'creating function delete_one_block' as action;
create or replace function delete_one_block (x varchar)
returns varchar
as $$
-- Delete all operations that would become orphans (i.e., not attached to a block) after that block's deletion
select delete_one_operation (operation_hash) from would_be_orphan_operations(x);

-- Update balance in case some operations remain alive after block is deleted
delete from balance where block_hash = x;

-- Deletions
delete from operation where block_hash = x;
delete from deactivated where block_hash = x;
delete from contract where block_hash = x;
delete from contract_balance where block_hash = x;

-- Delete block_alpha
delete from block_alpha where hash = x;
-- Finally, delete the block itself!
delete from block where hash = x;
select x;
$$ language SQL;

select 'creating function delete_rejected_blocks' as action;
create or replace function delete_rejected_blocks ()
returns varchar
as $$
select delete_one_block(b.hash) from block b where b.rejected;
$$ language SQL;

select 'creating function delete_rejected_blocks_and_above' as action;
create or replace function delete_rejected_blocks_and_above ()
returns integer
as $$
select delete_one_block(b.hash) from block b where b.level >= (select bb.level from block bb where bb.rejected order by level asc limit 1);
select level from block order by level desc limit 1;
$$ language SQL;
