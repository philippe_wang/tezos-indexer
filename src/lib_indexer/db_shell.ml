(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Env_V0 = Tezos_protocol_environment.MakeV0(
  struct let name = "Tezos_indexer_lib.Db_shell" end
  )()

open Caqti_type.Std

let caqti_or_fail =
  function
  | Error e as x ->
    Format.eprintf "Database error: %a\n%!" Caqti_error.pp e;
    print_endline __LOC__;
    Caqti_lwt.or_fail x
  | Ok e -> Lwt.return e

module Time = Time.Protocol

let connect url =
  Lwt.catch (fun () ->
      Caqti_lwt.connect url >>=
      caqti_or_fail
    ) (fun e ->
      Printf.eprintf "Database connection error: %s\n\
                      To get more information about specifying how to connect \
                      to the database, run:\n%s --help\n"
        (Printexc.to_string e)
        Sys.argv.(0);
      exit Verbose.ExitCodes.database_connection
    )


let to_caqti_error = function
  | Ok a -> Ok a
  | Error e -> Error (Format.asprintf "%a" pp_print_error e)

let with_transaction conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.exec request elt >>=
    caqti_or_fail
  end elts

let with_transaction_ignore conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.find_opt request elt >>=
    caqti_or_fail >>= fun _ ->
    Lwt.return_unit
  end elts

let with_transaction_pool pool request elts =
  Caqti_lwt.Pool.use
    (fun dbh ->
       let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
       let rec iter f = function
         | [] -> return ()
         | e::tl ->
           Lwt.bind
             (f e)
             (fun () -> iter f tl)
       in
       iter
         (fun elt -> Conn.exec request elt >>= caqti_or_fail >>= fun _ -> Lwt.return_unit)
         elts
       >>= fun _ ->
       Lwt.return (Ok ())
    ) pool >>= fun _ -> Lwt.return_unit


let find_opt pool f x =
  Caqti_lwt.Pool.use
    (fun dbh ->
      let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
      Conn.find_opt f x
    )
    pool

let chain_id =
  let open Chain_id in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let bh =
  let open Block_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let time =
  let open Time in
  custom
    ~encode:begin fun a ->
      match Ptime.of_float_s (Int64.to_float (to_seconds a)) with
      | None -> Error "time"
      | Some v -> Ok v
    end
    ~decode:(fun a -> Ok (of_seconds (Int64.of_float (Ptime.to_float_s a))))
    ptime

let fitness =
  custom
    ~encode:(fun a -> let `Hex a = Env_V0.MBytes.to_hex (Fitness.to_bytes a) in Ok a)
    ~decode:(fun a -> match Fitness.of_bytes (Env_V0.MBytes.of_string a) with
        | Some a -> Ok a
        | None -> Error "fitness")
    octets

let pkh =
  let open Signature.Public_key_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let pk =
  let open Signature.Public_key in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let operation_list_list_hash =
  let open Operation_list_list_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let context_hash =
  let open Tezos_crypto.Context_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let shell_header =
  let open Block_header in
  custom
    ~encode:begin fun {
      level ; proto_level ; predecessor ; timestamp ;
      validation_passes ; operations_hash ; fitness ; context } ->
      Ok ((level, proto_level, predecessor, timestamp),
          (validation_passes, operations_hash, fitness, context))
    end
    ~decode:begin fun
      ((level, proto_level, predecessor, timestamp),
       (validation_passes, operations_hash, fitness, context)) ->
      Ok { level ; proto_level ; predecessor ; timestamp ;
           validation_passes ; operations_hash ; fitness ; context }
    end
    (tup2
       (tup4 int32 int bh time)
       (tup4 int operation_list_list_hash fitness context_hash))

let json =
  custom
    ~encode:(fun json -> Ok (Data_encoding.Json.to_string json))
    ~decode:Data_encoding.Json.from_string
    string

(* let z =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits a) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a))))
 *     octets
 * let milligas =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits (Fpgas.unsafe_z_of_t a)) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a)) |> Fpgas.unsafe_t_of_z))
 *     octets *)

let z =
  custom
    ~encode:(fun a -> Ok (Z.to_string a))
    ~decode:(function a -> Ok (Z.of_string a))
    string

let milligas =
  custom
    ~encode:(fun a -> Ok (Z.to_string (Fpgas.unsafe_z_of_t a)))
    ~decode:(function a -> Ok (Z.of_string a |> Fpgas.unsafe_t_of_z))
    string

let oph =
  let open Operation_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

open Caqti_request

module Indexer_log = struct
  let record =
    create_p (tup2 string string) unit Caqti_mult.zero (fun _ ->
        "insert into indexer_log (version, argv) values ($1, $2)"
      )
end

module Chain_id_table = struct
  let insert =
    create_p chain_id unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into chain values (?)"
        | `Mysql -> "insert ignore into chain values (?)"
        | `Pgsql -> "insert into chain values (?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

module Block_table = struct
  let insert =
    create_p (tup2 bh shell_header) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into block values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore into block values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into block values (?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let select_max_level =
    create_p unit (option int32) Caqti_mult.one
      (fun _di -> "select max(level) from block")
end

module Operation_table = struct
  let insert =
    create_p (tup3 oph chain_id bh) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "insert into operation (hash, chain, block_hash) values ($1, $2, $3) on conflict (hash, block_hash) \
                     do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

module Deactivated_delegate_table = struct
  let insert =
    create_p (tup2 pkh bh) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into deactivated values (?, ?)"
        | `Mysql -> "insert ignore into deactivated values (?, ?)"
        | `Pgsql -> "insert into deactivated values (?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

module Implicit_table = struct
  let insert_discovered =
    create_p pkh unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into implicit (pkh) values (?)"
        | `Mysql -> "insert ignore into implicit (pkh) values (?)"
        | `Pgsql -> "insert into implicit (pkh) values (?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let upsert_activated =
    create_p (tup2 pkh oph) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "insert into implicit (pkh, activated) values (?, ?) \
                     on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end


module Snapshot_table = struct

  let update_snapshot =
    create_p (tup2 int int32) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into snapshot values (?, ?)"
        | `Mysql -> "insert ignore into snapshot values (?, ?)"
        | `Pgsql -> "insert into snapshot values (?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let store_snapshot_levels pool levels =
    with_transaction_pool pool update_snapshot levels

end

module Rejected_blocks = struct
  let mark =
    create_p int32 (tup2 bh int) Caqti_mult.zero_or_more
      begin fun _ ->
        "select * from mark_rejected_blocks($1)"
      end
  let delete =
    create_p unit int32 Caqti_mult.zero_or_one
      begin fun _ ->
        "select delete_rejected_blocks_and_above()"
      end


end
