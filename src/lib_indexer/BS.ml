(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module BS0 = Tezos_shell_services.Block_services.Make(Tezos_protocol_000_Ps9mPmXa.Protocol)(Tezos_protocol_001_PtCJ7pwo.Protocol)

module BS1 = Tezos_client_001_PtCJ7pwo.Alpha_client_context.Alpha_block_services
module BS2 = Tezos_client_002_PsYLVpVv.Alpha_client_context.Alpha_block_services
module BS3 = Tezos_client_003_PsddFKi3.Alpha_client_context.Alpha_block_services
module BS4 = Tezos_client_004_Pt24m4xi.Alpha_client_context.Alpha_block_services
module BS5 = Tezos_shell_services.Block_services.Make(Tezos_protocol_005_PsBabyM1.Protocol)(Tezos_protocol_005_PsBabyM1.Protocol)
module BS6 = Tezos_shell_services.Block_services.Make(Tezos_protocol_006_PsCARTHA.Protocol)(Tezos_protocol_006_PsCARTHA.Protocol)
module BS7 = Tezos_shell_services.Block_services.Make(Tezos_protocol_007_PsDELPH1.Protocol)(Tezos_protocol_007_PsDELPH1.Protocol)
module BS8 = Tezos_shell_services.Block_services.Make(Tezos_protocol_008_PtEdoTez.Protocol)(Tezos_protocol_008_PtEdoTez.Protocol)

module BST12 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_001_PtCJ7pwo.Protocol)(Tezos_protocol_002_PsYLVpVv.Protocol)

module BST23 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_002_PsYLVpVv.Protocol)(Tezos_protocol_003_PsddFKi3.Protocol)

module BST34 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_003_PsddFKi3.Protocol)(Tezos_protocol_004_Pt24m4xi.Protocol)

module BST45 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_004_Pt24m4xi.Protocol)(Tezos_protocol_005_PsBabyM1.Protocol)

module BST56 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_005_PsBabyM1.Protocol)(Tezos_protocol_006_PsCARTHA.Protocol)

module BST67 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_006_PsCARTHA.Protocol)(Tezos_protocol_007_PsDELPH1.Protocol)

module BST78 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_007_PsDELPH1.Protocol)(Tezos_protocol_008_PtEdoTez.Protocol)
